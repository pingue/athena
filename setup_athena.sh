echo "This is the setup script for the 'Athena' debugging toolbox. \
By this point, you should have run the primary setup.sh to install \
common packages and working environment: https://github.com/Pingue/setup . \
This script will install the relevant packages required, and set up \
a default config. While this script is aimed at CentOS 7, it may work \
on other flavours of Linux, however no guarantees are made."
echo
while [ "$var" != "y" ]; do
    echo -n "Continue? [y|n] "
    read var
    if [ "$var" == "n" ]; then
        echo "Exiting on user request..."
        exit 1
    fi
done


user=`whoami`
if [ "$user" != "root" ]; then
	echo "This script must be run as root to continue! Exiting..."
	exit 1
fi
cd ${HOME}

# Random stuff
yum install -y ntfsprogs firefox gimp

# Screensaver off
xset s off
xset s noblank

# Install wireshark
yum install -y wireshark wireshark-gnome

# Setup hotspot
nmcli connection add type 802-11-wireless ifname wlp2s0 autoconnect no con-name Hotspot mode ap ssid "There is an island" ip4 10.42.0.1/24
nmcli connection modify Hotspot 802-11-wireless.band bg ipv4.method shared
nmcli connection modify Hotspot wifi-sec.key-mgmt wpa-psk
nmcli connection modify Hotspot wifi-sec.psk "whereriversrundeep"

# Install Squid / TinyProxy
yum install -y squid tinyproxy

# Install Remote Desktop
yum install -y freerdp

# Install ApacheBench
yum install -y httpd-tools

# Setup reverse tunnel service
cat >>/etc/systemd/system/tunnel.service <<EOF
[Unit]
Description=tunnel
After=network.target

[Service]
ExecStart=/bin/autossh -f -M 33203 -N -i /home/michael/.ssh/id_rsa -R 12123:localhost:22 michael@aura.mfrost.co.uk
Type=forking
Restart=on-abort

[Install]
WantedBy=default.target
EOF

# Install samba
yum install -y samba
mkdir /data
cat >>/etc/samba/smb.conf <<EOF
[data]
comment = Shared Data
path = /data
writable = no
public = yes
EOF

# Install NFS
yum install -y nfs-utils
cat >>/etc/exports <<EOF
/data *(ro)
EOF

# Install ldapsearch
yum install -y openldap-clients 

# Install Apache
yum install -y httpd

# Install rsyslog
yum install -y rsyslog

# Install mysql
yum install -y mariadb-server

# Install iscsi tools
yum install -y iscsi-initiator-utils
mkdir /iscsi
echo "InitiatorName=iqn.2008-09.uk.co.mfrost:athena" > /etc/iscsi/initiatorname.iscsi
targetcli /backstores/fileio create file_or_dev=/iscsi/default.block name=default size=1G sparse=true
targetcli /iscsi create iqn.2008-09.uk.co.mfrost:athena
targetcli /iscsi/iqn.2008-09.uk.co.mfrost:athena/tpg1/luns create lun=1 storage_object=/backstores/fileio/default
targetcli saveconfig

# Setup NTP
echo "allow 0.0.0.0/0" >> /etc/chrony.conf
systemctl restart chronyd
systemctl enable chronyd

# Install clam
yum install -y clamav clamav-update
sed -i "s/Example/#Example/" /etc/freshclam.conf

#TODO: Setup as per https://www.clamav.net/documents/private-local-mirrors
#Set up a virtualhost in apache to handle this, and a freshclam to download to it
echo "TODO: STUFF"

# Install other tools
yum install -i chromium nmap conky lm_sensors
sensors-detect
#TODO: Config conky to be transparant, include sensor data etc etc
#TODO: Config conky to start on boot
#TODO: Config conky to show network info etc. See doc

# Install bind
yum install -y bind bind-utils
sed -i "s/listen-on port 53 { 127.0.0.1; };/listen-on port 53 { any; };/" /etc/named.conf
sed -i "s/allow-query     { localhost; };/allow-query     { any; };/" /etc/named.conf

# Install dhcpd
yum install -y dhcp
nmcli connection add type ethernet con-name dhcp_host ifname enp1s0 ip4 10.42.0.1/24
# dhcpd conf, and the sysconfig dhcpd file

# Install tftp server
yum install tftp-server tftp xinetd -y
mkdir -p /tftpboot
sed -i 's|/var/lib/tftpboot|/tftpboot|' /etc/xinetd.d/tftp
sed -i 's|/var/lib/tftpboot|/tftpboot|' /usr/lib/systemd/system/tftp.service
sed -i "s/\(disable.*\)yes/\1no/" /etc/xinetd.d/tftp
#setsebool -P tftp_anon_write 1
#Change the label on /tftpboot
rmdir /var/lib/tftpboot/
ln -s /tftpboot/ /var/lib/tftpboot
chcon -t tftpdir_rw_t /tftpboot
semanage fcontext -a -t tftpdir_rw_t "/tftpboot(/.*)?"
restorecon -rv /tftpboot/

# Setup PXE structure
yum install -y syslinux
cp /usr/share/syslinux/pxelinux.0 /tftpboot
cp /usr/share/syslinux/menu.c32 /tftpboot
cp /usr/share/syslinux/memdisk /tftpboot
cp /usr/share/syslinux/mboot.c32 /tftpboot
cp /usr/share/syslinux/chain.c32 /tftpboot
mkdir -p /tftpboot/pxelinux.cfg
mkdir -p /tftpboot/images/centos/x86_64/7
mkdir -p /var/www/html/mirrors
# pxeboot menu file
echo "Copy files across for the PXE images"

# Install Kismet
yum install -y pcre pcre-devel gcc libstdc++-devel.x86_64 gcc-c++ ncurses-libs ncurses-devel libpcap-devel libnl-devel
cd ~
wget http://www.kismetwireless.net/code/kismet-2016-07-R1.tar.xz
tar -xvf kismet-2016-02-R1.tar.xz
cd kismet-2016-07-R1
./configure
make dep
make
make install
systemctl disable iscsi

# Install wavemon
yum install -y wavemon

# Install VNC server
yum install tigervnc-server xorg-x11-fonts-Type1 -y
cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@:1.service
sed -i "s/<USER>/michael/" /etc/systemd/system/vncserver@:1.service
systemctl daemon-reload
echo "ENTER PASSWORD FOR VNC SERVER (michael)"
sudo -u michael vncserver

# Install nethogs
yum install -y nethogs

# Install lrzsz for serial tx
yum install -y lrzsz

# Wrap up
echo "Don't forget to copy across your private key!!!"

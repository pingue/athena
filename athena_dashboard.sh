success=0
user=`whoami`
if [ "$user" != "root" ]; then
	echo "Athena must be run as root"
	exit 1
fi

echo "========================="
echo "Athena"
echo "========================="
echo
echo "1) Wireshark"
echo "2) Enable Hotspot"
echo "3) Disable Hotspot"
echo "4) Start SSHD"
echo "5) Stop SSHD"
echo "6) Start Reverse Tunnel"
echo "7) Start Squid"
echo "8) Stop Squid"
echo "9) Start TinyProxy"
echo "10) Stop TinyProxy"
echo "11) FreeRDP. I don't do anything"
echo "12) ApacheBench. I don't do anything"
echo "13) Start Samba. SELINUX"
echo "14) Stop Samba"
echo "15) Start NFS"
echo "16) Stop NFS"
echo "17) LDAP Search. I don't do anything"
echo "18) Start HTTPD"
echo "19) Stop HTTPD"
echo "20) Start MySQL"
echo "21) Stop MySQL"
echo "22) Start iSCSI. I don't work. Errors when trying to login. Something something ACL?"
echo "23) Stop iSCSI"
echo "24) Enable NTP"
echo "25) Disable NTP"
echo "26) Run local clam scan"
echo "27) Enable DNS"
echo "28) Disable DNS"
echo "29) Enable DHCPD"
echo "30) Disable DHCPD"
echo "31) Enable TFTP"
echo "32) Disable TFTP"
echo "33) Netboot. INCOMPLETE"
echo "34) Wavemon"
echo "35) Start VNC. Work out why I throw an error"
echo "36) Stop VNC"
echo "37) Start LDAP Server. NOT IMPLEMENTED"
echo "38) Stop LDAP Server. NOT IMPLEMENTED"
echo "39) Start postfix Server. NOT IMPLEMENTED"
echo "40) Stop postfix Server. NOT IMPLEMENTED"
echo "41) Run nethogs"
echo "42) Serial file transfer. INFORM ONLY"

while [ $success == 0 ]; do
echo
echo -n "Choice > "
read entry

case $entry in
    1) 
        /sbin/wireshark
        success=1
        ;;
    2)
        nmcli connection up dhcp_client
        nmcli connection up Hotspot
        success=1
        ;;
    3)
        nmcli connection down Hotspot
        success=1
        ;;
    4)
        systemctl start sshd
        success=1
        ;;
    5)
        systemctl stop sshd
        success=1
        ;;
    6)
        systemctl start sshd
        firewall-cmd --add-service=ssh
        systemctl start tunnel
        success=1
        ;;
    7)
        firewall-cmd --add-port=3128/tcp
        systemctl start squid
        success=1
        ;;
    8) 
        systemctl stop squid
        firewall-cmd --remove-port=3128/tcp
        success=1
        ;;
    9)
        firewall-cmd --add-port=8888/tcp
        systemctl start tinyproxy
        echo "tail -f /var/log/tinyproxy/tinyproxy.log"
        success=1
        ;;
    10)
        systemctl stop tinyproxy
        firewall-cmd --remove-port=8888/tcp
        success=1
        ;;
    11)
        echo "I should give a quick copy-paste on an RDP command"
        success=1
        ;;
    12) 
        echo "I should give a quick copy-paste on ApacheBench"
        success=1
        ;;
    13) 
        systemctl start smb
        firewall-cmd --add-service=samba
        success=1
        ;;
    14) 
        systemctl stop smb
        firewall-cmd --remove-service=samba
        success=1
        ;;
    15) 
        systemctl start nfs-server
        firewall-cmd --add-service=nfs
        success=1
        ;;
    16) 
        systemctl stop nfs-server
        firewall-cmd --remove-service=nfs
        success=1
        ;;
    17) 
        echo "I should give a quick copy-paste on ldapsearch"
        success=1
        ;;
    18)
        systemctl start httpd
        firewall-cmd --add-service=http
        firewall-cmd --add-service=https
        success=1
        ;;
    19) 
        systemctl stop httpd
        firewall-cmd --remove-service=http
        firewall-cmd --remove-service=https
        success=1
        ;;
    20)
        systemctl start mariadb
        firewall-cmd --add-service=mysql
        success=1
        ;;
    21) 
        systemctl stop mariadb
        firewall-cmd --remove-service=mysql
        success=1
        ;;
    22) 
        firewall-cmd --add-port=3260/tcp
        echo "iscsiadm --mode discoverydb --type sendtargets --portal localhost --discover"
        echo "iscsiadm --mode node --targetname iqn.2008-09.uk.co.mfrost:athena --portal localhost:3260 --login"
        success=1
        ;;
    23) 
        firewall-cmd --remove-port=3260/tcp
        success=1
        ;;
    24) 
        firewall-cmd --add-service=ntp
        success=1
        ;;
    25) 
        firewall-cmd --remove-service=ntp
        success=1
        ;;
    26) 
        freshclam
        clamscan -or / | grep -ve "Symbolic link" -ve "Empty file"
        success=1
        ;;
    27)
        systemctl start named
        firewall-cmd --add-service=dns
        success=1
        ;;
    28) 
        systemctl stop named
        firewall-cmd --remove-service=dns
        success=1
        ;;
    29) 
        nmcli con up dhcp_host
        firewall-cmd --add-service=dhcp
        systemctl start dhcpd
        success=1
        ;;
    30) 
        nmcli con down dhcp_host
        firewall-cmd --remove-service=dhcp
        systemctl stop named
        success=1
        ;;
    31) 
        firewall-cmd --add-service=tftp
        systemctl start xinetd
        systemctl start tftp
        success=1
        ;;
    32) 
        firewall-cmd --remove-service=tftp
        systemctl stop tftp
        systemctl stop xinetd
        success=1
        ;;
    33)
        systemctl start httpd
        firewall-cmd --add-service=http
        firewall-cmd --add-service=https
        nmcli con up dhcp_host
        firewall-cmd --add-service=dhcp
        systemctl start dhcpd
        firewall-cmd --add-service=tftp
        systemctl start xinetd
        systemctl start tftp
        success=1
        ;;
    34)
        wavemon
        success=1
        ;;
    35)
        firewall-cmd --add-port=5903/tcp
        systemctl start vncserver@:3.service
        success=1
        ;;
    36)
        systemctl stop vncserver@:3.service
        firewall-cmd --remove-port=5903/tcp
        success=1
        ;;
    37)
        echo "Start LDAP server"
        success=1
        ;;
    38)
        echo "Stop LDAP server"
        success=1
        ;;
    39)
        echo "Start postfix server"
        success=1
        ;;
    40)
        echo "Stop postfix server"
        success=1
        ;;
    41)
        echo "Starting nethogs"
        nethogs
        success=1
        ;;
    42)
        echo "rz to receive a file over serial, sz to send one"
        success=1
        ;;
    *) 
        success=0
    esac
done

exit 0
